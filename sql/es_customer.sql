/*
 Navicat Premium Data Transfer

 Source Server         : localhost
 Source Server Type    : MySQL
 Source Server Version : 80019
 Source Host           : localhost:3306
 Source Schema         : estore

 Target Server Type    : MySQL
 Target Server Version : 80019
 File Encoding         : 65001

 Date: 28/12/2022 11:20:38
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for es_customer
-- ----------------------------
DROP TABLE IF EXISTS `es_customer`;
CREATE TABLE `es_customer`  (
  `id` int NOT NULL AUTO_INCREMENT COMMENT '客户id',
  `name` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户名字',
  `password` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户密码',
  `zipCode` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户邮编',
  `address` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户地址',
  `telephone` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户电话',
  `email` varchar(255) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT NULL COMMENT '客户邮箱',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 16 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = DYNAMIC;

-- ----------------------------
-- Records of es_customer
-- ----------------------------
INSERT INTO `es_customer` VALUES (1, '张三风', '1212121221', '453635', '河南省南阳市', '13212122112', '23232@qq.com');
INSERT INTO `es_customer` VALUES (2, '李四', '322323', '453635', '河南省南阳市', '13212122112', '23232@qq.com');
INSERT INTO `es_customer` VALUES (3, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (4, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (5, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (6, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (7, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (8, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (9, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (10, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (11, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (12, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (13, 'JamieChyi', '1212', '121211', '广州市白云区', '13234343434', '27222@126.com');
INSERT INTO `es_customer` VALUES (14, '丽丽', '211314', '474150', '河南省邓州市', '13212121212', '2323232@qq.com');
INSERT INTO `es_customer` VALUES (15, '高军', '232323', '121212', '河南省信阳市', '13265656565', 'gaojun@126.com');

SET FOREIGN_KEY_CHECKS = 1;
