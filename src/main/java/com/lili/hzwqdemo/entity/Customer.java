package com.lili.hzwqdemo.entity;

import io.swagger.annotations.ApiModelProperty;

/**
 * 客户实体类
 *
 * @author QiJingJing
 * @create 2022/12/22
 */
public class Customer {
    @ApiModelProperty("客户编号")
    private Long id;
    @ApiModelProperty("客户姓名")
    private String name;
    @ApiModelProperty("客户密码")
    private String password;
    @ApiModelProperty("客户邮编")
    private String zipCode;
    @ApiModelProperty("客户地址")
    private String address;
    @ApiModelProperty("客户电话")
    private String telephone;
    @ApiModelProperty("客户邮箱")
    private String email;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getZipCode() {
        return zipCode;
    }

    public void setZipCode(String zipCode) {
        this.zipCode = zipCode;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
}
