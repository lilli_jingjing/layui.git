package com.lili.hzwqdemo.util;

/**
 * @author QiJingJing
 */
public enum AppHttpCodeEnum {
    /**
     * 操作成功
     */
    SUCCESS(0,"操作成功");
    final int code;
    final String msg;

    AppHttpCodeEnum(int code, String errorMessage){
        this.code = code;
        this.msg = errorMessage;
    }

    public int getCode() {
        return code;
    }

    public String getMsg() {
        return msg;
    }
}
