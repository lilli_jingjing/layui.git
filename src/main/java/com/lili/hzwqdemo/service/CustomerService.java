package com.lili.hzwqdemo.service;

import com.lili.hzwqdemo.entity.Customer;
import com.lili.hzwqdemo.util.ResponseResult;

import java.util.List;

/**
 * customer类服务层
 * @author QiJingJing
 * @create 2022/12/22
 */
public interface CustomerService {
    /**
     * 条件查询所有信息
     *
     * @author qijingjing
     * @date 2022/12/28
     * @param page
     * @param limit
     * @param name
     * @param email
     * @return com.lili.hzwqdemo.util.ResponseResult
     **/
    ResponseResult findCustomerAll(Integer page, Integer limit,String name,String email);
    /**
     * 根据id查询用户信息
     *
     * @author qijingjing
     * @date 2022/12/22
     * @param id
     * @return com.lili.hzwqdemo.entity.Customer
     **/
    Customer findCustomerById(Long id);
    /**
     *  添加一个客户信息
     *
     * @author qijingjing
     * @date 2022/12/22
     * @param customer
     * @return java.lang.Boolean
     **/
    Boolean saveCustomer(Customer customer);
    /**
     * 修改客户信息
     *
     * @author qijingjing
     * @date 2022/12/22
     * @param customer
     * @return java.lang.Boolean
     **/
    Boolean updateCustomer(Customer customer);
    /**
     * 删除客户信息
     *
     * @author qijingjing
     * @date 2022/12/22
     * @param id
     * @return java.lang.Boolean
     **/
    Boolean delete(Long id);
}
