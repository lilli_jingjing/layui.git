package com.lili.hzwqdemo.service.impl;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.lili.hzwqdemo.entity.Customer;
import com.lili.hzwqdemo.mapper.CustomerMapper;
import com.lili.hzwqdemo.service.CustomerService;
import com.lili.hzwqdemo.util.ResponseResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @author QiJingJing
 * @create 2022/12/22
 */
@Service
public class CustomerServiceImpl implements CustomerService {
    @Autowired
    private CustomerMapper customerMapper;
    @Override
    public ResponseResult findCustomerAll(Integer page, Integer limit,String name,String email) {
        Page<Customer> customerPage = PageHelper.startPage(page, limit);
        customerMapper.findCustomerAll(name,email);
        ResponseResult responseResult = ResponseResult.okResult(customerPage.getResult());
        responseResult.setCount(customerPage.getTotal());
        return responseResult;
    }

    @Override
    public Customer findCustomerById(Long id) {
        return customerMapper.findCustomerById(id);
    }

    @Override
    public Boolean saveCustomer(Customer customer) {
        return customerMapper.saveCustomer(customer);
    }

    @Override
    public Boolean updateCustomer(Customer customer) {
        return customerMapper.updateCustomer(customer);
    }

    @Override
    public Boolean delete(Long id) {

        return customerMapper.delete(id);
    }
}
