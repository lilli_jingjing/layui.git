package com.lili.hzwqdemo.mapper;

import com.lili.hzwqdemo.entity.Customer;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * @author QiJingJing
 * @date  2022/12/22
 */
@Mapper
@Repository
public interface CustomerMapper {
    /**
     * 查询所有客户信息
     *
     * @author qijingjing
     * @date 2022/12/29
     * @param name 客户姓名
     * @param email 客户邮箱
     * @return java.util.List<com.lili.hzwqdemo.entity.Customer>
     **/
    List<Customer> findCustomerAll(@Param("name") String name, @Param("email") String email);
    /**
     * 根据id查询客户信息
     *
     * @author qijingjing
     * @date 2022/12/22
     * @param id 客户id
     * @return com.lili.hzwqdemo.entity.Customer
     **/
    Customer findCustomerById(@Param("id") Long id);
    /**
     * 新增一个客户信息
     *
     * @author qijingjing
     * @date 2022/12/29
     * @param customer 对象实体
     * @return java.lang.Boolean
     **/
    Boolean saveCustomer(Customer customer);
    /**
     * 更新用户信息
     *
     * @author qijingjing
     * @date 2022/12/29
     * @param customer 客户实体
     * @return java.lang.Boolean
     **/
    Boolean updateCustomer(Customer customer);
    /**
     * 根据id删除一个客户信息
     *
     * @author qijingjing
     * @date 2022/12/29
     * @param id 客户id
     * @return java.lang.Boolean
     **/
    Boolean delete(@Param("id") Long id);
}
