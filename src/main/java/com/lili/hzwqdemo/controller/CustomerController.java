package com.lili.hzwqdemo.controller;

import com.lili.hzwqdemo.entity.Customer;
import com.lili.hzwqdemo.service.CustomerService;
import com.lili.hzwqdemo.util.ResponseResult;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiParam;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * (Customer表控制层)
 * @author QiJingJing
 * @create 2022/12/22
 */
@CrossOrigin
@RestController
@Api(tags = "customer表相关接口")
@RequestMapping("customer")
public class CustomerController {
    @Autowired
    private CustomerService customerService;
    @GetMapping("list")
    @ApiOperation("查询所有用户信息")
    ResponseResult findCustomerAll(@RequestParam("page") Integer page,@RequestParam("limit") Integer limit,@RequestParam("name") String name,@RequestParam("email") String email){
        return customerService.findCustomerAll(page,limit,name,email);
    }
    @GetMapping("getOne")
    @ApiOperation("根据id查询用户信息")
    Customer findCustomerById(@ApiParam("客户编号") Long id){
        return customerService.findCustomerById(id);
    }
    @PostMapping("add")
    @ApiOperation("添加一个客户信息")
    ResponseResult saveCustomer(@RequestBody Customer customer){
         customerService.saveCustomer(customer);
         return ResponseResult.okResult();
    }
    @PostMapping("update")
    @ApiOperation("修改客户信息")
    ResponseResult updateCustomer(@RequestBody Customer customer){
         customerService.updateCustomer(customer);
         return ResponseResult.okResult();
    }
    @PostMapping("delete")
    @ApiOperation("删除客户信息")
    ResponseResult delete(@ApiParam("客户编号") @RequestParam("id") Long id){
         customerService.delete(id);
         return ResponseResult.okResult();
    }
}
