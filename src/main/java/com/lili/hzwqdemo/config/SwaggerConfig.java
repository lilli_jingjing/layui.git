package com.lili.hzwqdemo.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import springfox.documentation.oas.annotations.EnableOpenApi;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author QiJingJing
 * @create 2022/12/22
 */
@EnableWebMvc
@EnableOpenApi
@Configuration
public class SwaggerConfig {
}
