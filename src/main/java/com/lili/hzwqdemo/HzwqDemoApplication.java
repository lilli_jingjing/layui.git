package com.lili.hzwqdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HzwqDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(HzwqDemoApplication.class, args);
    }

}
